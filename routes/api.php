<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => 'api',
        'prefix' => 'auth',

    ],
    static function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('user-profile', 'AuthController@userProfile');
    }
);
Route::group(
    ['middleware' => 'auth:api'],
    function () {
        Route::get('todos', 'TodoController@index');
        Route::get('todos/{todo}', 'TodoController@show');
        Route::post('todos', 'TodoController@store');
        Route::put('todos/{todo}', 'TodoController@update');
        Route::delete('todos/{todo}', 'TodoController@delete');

        Route::get('projects', 'ProjectController@index');
        Route::get('projects/{project}', 'ProjectController@show');
        Route::post('projects', 'ProjectController@store');
        Route::put('projects/{project}', 'ProjectController@update');
        Route::delete('projects/{project}', 'ProjectController@delete');

        Route::get('users', 'UserController@index');
        Route::get('users/{user}', 'UserController@show');
    }
);
