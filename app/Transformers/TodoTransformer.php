<?php

namespace App\Transformers;

use App\Todo;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class TodoTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'user',
        'project',
    ];

    public function transform(Todo $todo): array
    {
        return [
            'id' => $todo->id,
            'description' => $todo->description,
            'done' => $todo->done,
            'created_at' => $todo->created_at,
            'updated_at' => $todo->updated_at,
        ];
    }

    public function includeUser(Todo $todo): ?Item
    {
        return $this->item($todo->user, new UserTransformer());
    }

    public function includeProject(Todo $todo): ?Item
    {
        return $this->item($todo->project, new ProjectTransformer());
    }
}
