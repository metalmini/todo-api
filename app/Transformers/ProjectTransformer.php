<?php

namespace App\Transformers;

use App\Project;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'todos',
    ];

    public function transform(Project $project): array
    {
        return [
            'id' => $project->id,
            'name' => $project->name,
            'created_at' => $project->created_at,
            'updated_at' => $project->updated_at,
        ];
    }

    public function includeTodos(Project $project): ?Collection
    {
        return $this->collection($project->todos, new TodoTransformer());
    }
}
