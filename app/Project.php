<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed id
 * @property mixed name
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed todos
 */
class Project extends Model
{
    protected $fillable = ['name'];

    public function todos(): hasMany
    {
        return $this->hasMany(Todo::class);
    }
}
