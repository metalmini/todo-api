<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static create(array $all)
 * @property mixed id
 * @property mixed description
 * @property mixed done
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed user
 * @property mixed project
 * @property mixed user_id
 * @property mixed project_id
 */
class Todo extends Model
{
    protected $fillable = ['description', 'done', 'user_id', 'project_id'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }
}
