<?php

namespace App\Http\Controllers;

use App\Transformers\UserTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Spatie\Fractal\Facades\Fractal;
use Validator;
use App\User;

/**
 * @group  Authentication Calls
 */
class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * @bodyParam  email string required Email address from user.
     * @bodyParam  password string required Password from user.
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email',
                'password' => 'required|string|min:6',
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    /**
     * @bodyParam  email string required Email address from user.
     * @bodyParam  name string required Name of user.
     * @bodyParam  password string required Password from user.
     * @bodyParam  password_confirmation string required Password confirmation from user.
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string|between:2,100',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|string|confirmed|min:6',
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(
            array_merge(
                $validator->validated(),
                ['password' => bcrypt($request->password)]
            )
        );

        return response()->json(
            fractal($user)->transformWith(new UserTransformer())->parseExcludes('project.todos, user.todos'),
            201
        );
    }

    /**
     * @authenticated
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * @authenticated
     */
    public function refresh(): JsonResponse
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * @transformer  \App\Transformers\UserTransformer
     * @authenticated
     */
    public function userProfile(): array
    {
        return Fractal::create()
            ->item(auth()->user(), new UserTransformer(), 'user')
            ->parseExcludes('todos.user, todos.project')
            ->toArray();
    }

    protected function createNewToken(string $token): JsonResponse
    {
        return response()->json(
            [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60,
                'user' => auth()->user(),
            ]
        );
    }

}
