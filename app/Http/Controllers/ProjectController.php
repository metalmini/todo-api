<?php

namespace App\Http\Controllers;

use App\Project;
use App\Transformers\ProjectTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

/**
 * @group  Project Calls
 * @authenticated
 */
class ProjectController extends Controller
{
    /**
     * @transformercollection  \App\Transformers\ProjectTransformer
     * @transformerModel  \App\Project
     */
    public function index(): array
    {
        return Fractal::create()
            ->collection(Project::all(), new ProjectTransformer(), 'projects')
            ->parseExcludes('todos.project, todos.user')
            ->toArray();
    }

    /**
     * @transformer  \App\Transformers\ProjectTransformer
     */
    public function show(Project $project): array
    {
        return Fractal::create()
            ->item($project, new ProjectTransformer(), 'project')
            ->parseExcludes('todos.project, todos.user')
            ->toArray();
    }

    /**
     * @bodyParam  name string required Name of project.
     * @transformer  \App\Transformers\ProjectTransformer
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate(
            [
                'name' => 'required',
            ]
        );
        $project = Project::create($request->all());

        return response()->json(
            fractal($project)->transformWith(new ProjectTransformer())->parseExcludes('todos.project'),
            201
        );
    }

    /**
     * @bodyParam  name string Name of project.
     * @transformer  \App\Transformers\ProjectTransformer
     */
    public function update(Request $request, Project $project): JsonResponse
    {
        $project->update($request->all());

        return response()->json(
            fractal($project)->transformWith(new ProjectTransformer())->parseExcludes('todos.project'),
            200
        );
    }

    public function delete(Project $project)
    {
        $project->delete();

        return response()->json(null, 204);
    }
}
