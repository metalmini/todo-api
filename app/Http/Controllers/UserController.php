<?php

namespace App\Http\Controllers;

use App\Transformers\UserTransformer;
use App\User;
use Spatie\Fractal\Fractal;

/**
 * @group  User Calls
 * @authenticated
 */
class UserController extends Controller
{
    /**
     * @transformercollection  \App\Transformers\UserTransformer
     * @transformerModel  \App\User
     */
    public function index(): array
    {
        return Fractal::create()
            ->collection(User::all(), new UserTransformer(), 'todos')
            ->parseExcludes('todos.user, todos.project')
            ->toArray();
    }

    /**
     * @transformer  \App\Transformers\UserTransformer
     */
    public function show(User $user): array
    {
        return Fractal::create()
            ->item($user, new UserTransformer(), 'project')
            ->parseExcludes('todos.user, todos.project')
            ->toArray();
    }
}
