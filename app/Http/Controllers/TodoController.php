<?php

namespace App\Http\Controllers;

use App\Todo;
use App\Transformers\TodoTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Item;
use Spatie\Fractal\Fractal;

/**
 * @group  Todo Calls
 * @authenticated
 */
class TodoController extends Controller
{
    /**
     * @transformercollection  \App\Transformers\TodoTransformer
     * @transformerModel  \App\Todo
     */
    public function index(): array
    {
        return Fractal::create()
            ->collection(Todo::all(), new TodoTransformer(), 'todos')
            ->parseExcludes('project.todos, user.todos')
            ->toArray();
    }

    /**
     * @transformer  \App\Transformers\TodoTransformer
     */
    public function show(Todo $todo): array
    {
        return Fractal::create()
            ->item($todo, new TodoTransformer(), 'todo')
            ->parseExcludes('project.todos, user.todos')
            ->toArray();
    }

    /**
     * @bodyParam  description string required Description of todo.
     * @bodyParam  done boolean required Status of todo.
     * @bodyParam  project_id int required Related project.
     * @transformer  \App\Transformers\TodoTransformer
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'description' => 'required',
                'done' => 'boolean',
                'project_id' => 'required|int',
            ]
        );

        $todo = new Todo();
        $todo->description = $request->description;
        $todo->done = $request->done;
        $todo->project_id = $request->project_id;
        $todo->user_id = Auth::user()->id;
        $todo->save();

        return response()->json(
            fractal($todo)->transformWith(new TodoTransformer())->parseExcludes('project.todos, user.todos'),
            201
        );
    }

    /**
     * @bodyParam  description string Description of todo.
     * @bodyParam  done boolean Status of todo.
     * @bodyParam  project_id int Related project.
     * @transformer  \App\Transformers\TodoTransformer
     */
    public function update(Request $request, Todo $todo)
    {
        if ($todo->user->id !== Auth::user()->id) {
            abort(403, 'Access denied');
        }

        $todo->update($request->all());

        return response()->json(
            fractal($todo)->transformWith(new TodoTransformer())->parseExcludes('project.todos, user.todos'),
            200
        );
    }

    public function delete(Todo $todo)
    {
        if ($todo->user->id !== Auth::user()->id) {
            abort(403, 'Access denied');
        }

        $todo->delete();

        return response()->json(null, 204);
    }
}
