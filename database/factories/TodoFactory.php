<?php

/** @var Factory $factory */

use App\Todo;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence,
        'done' => $faker->boolean,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'project_id' => function () {
            return factory(App\Project::class)->create()->id;
        },
    ];
});

