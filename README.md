### Performance opdracht

####  How to run

First of all we need to clone the project

`git clone git@bitbucket.org:metalmini/todo-api.git`

Create a new database and edit your .env file to match that

Run composer install to get all the vendor libs

`composer install`

Run the migrations and seed some dummy data to get started

`php artisan migrate --seed`

We want to use JWT tokens for authentication, so we need to generate a secret

`php artisan jwt:secret`

When its all done, we are ready to serve the application

`php artisan serve`

Documentation for the API can be found here:

http://127.0.0.1:8000/docs/


####  Briefing

Het doel van deze opdracht is om een API in laravel te bouwen voor een Todo applicatie. In deze applicatie kan de client een todo aanmaken voor een specifiek project. Een todo is gekoppeld aan een project en een gebruiker. Todos kunnen gemarkeerd worden als “done”. Todos kunnen verwijderd worden.

De volgende requirements zijn belangrijk voor deze opdracht:

**Project**
-	Een project kan aangemaakt worden
-	een project heeft een naam
-	een project kan meerdere todos hebben

**Todo**
-	Een todo kan aangemaakt worden
-	een todo heeft een beschrijving
-	een todo kan “niet klaar” en “klaar” zijn (default “niet klaar”)
-	een todo is altijd gekoppeld aan een project
-	een todo is altijd gekoppeld aan een user
-	een todo kan als klaar gemarkeerd worden
-	een todo kan verwijderd worden
-	er is een lijst van todos te raadplegen voor een project


Dit zijn de requirements voor de API. De source code voor deze applicatie kan gedeeld worden via een zip file of een link naar de github repository.
