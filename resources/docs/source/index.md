---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Authentication Calls


<!-- START_a925a8d22b3615f12fca79456d286859 -->
## api/auth/login
> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"eius","password":"nihil"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "eius",
    "password": "nihil"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | Email address from user.
        `password` | string |  required  | Password from user.
    
<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_2e1c96dcffcfe7e0eb58d6408f1d619e -->
## api/auth/register
> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"quis","name":"cum","password":"iste","password_confirmation":"et"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "quis",
    "name": "cum",
    "password": "iste",
    "password_confirmation": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/register`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | Email address from user.
        `name` | string |  required  | Name of user.
        `password` | string |  required  | Password from user.
        `password_confirmation` | string |  required  | Password confirmation from user.
    
<!-- END_2e1c96dcffcfe7e0eb58d6408f1d619e -->

<!-- START_19ff1b6f8ce19d3c444e9b518e8f7160 -->
## api/auth/logout
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/logout`


<!-- END_19ff1b6f8ce19d3c444e9b518e8f7160 -->

<!-- START_994af8f47e3039ba6d6d67c09dd9e415 -->
## api/auth/refresh
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/refresh`


<!-- END_994af8f47e3039ba6d6d67c09dd9e415 -->

<!-- START_47c445190447d890792ca1d550cfdbd0 -->
## api/auth/user-profile
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/auth/user-profile" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/user-profile"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "name": "Abelardo Bayer",
        "email": "amir.roob@example.com",
        "email_verified_at": "2020-07-21T15:37:28.000000Z",
        "created_at": null,
        "updated_at": null,
        "todos": {
            "data": []
        }
    }
}
```

### HTTP Request
`GET api/auth/user-profile`


<!-- END_47c445190447d890792ca1d550cfdbd0 -->

#Project Calls


<!-- START_893ae955e8991ef06f6de91adbff0aaa -->
## api/projects
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "name": "illum",
            "created_at": null,
            "updated_at": null,
            "todos": {
                "data": []
            }
        },
        {
            "id": null,
            "name": "blanditiis",
            "created_at": null,
            "updated_at": null,
            "todos": {
                "data": []
            }
        }
    ]
}
```

### HTTP Request
`GET api/projects`


<!-- END_893ae955e8991ef06f6de91adbff0aaa -->

<!-- START_62d96e2c27434ddb7c604817f783bed8 -->
## api/projects/{project}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "name": "perferendis",
        "created_at": null,
        "updated_at": null,
        "todos": {
            "data": []
        }
    }
}
```

### HTTP Request
`GET api/projects/{project}`


<!-- END_62d96e2c27434ddb7c604817f783bed8 -->

<!-- START_d1a366aa47ee59c96780bfe89ca95bdd -->
## api/projects
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://localhost/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"culpa"}'

```

```javascript
const url = new URL(
    "http://localhost/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "culpa"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "name": "dolorem",
        "created_at": null,
        "updated_at": null,
        "todos": {
            "data": []
        }
    }
}
```

### HTTP Request
`POST api/projects`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Name of project.
    
<!-- END_d1a366aa47ee59c96780bfe89ca95bdd -->

<!-- START_ef65c5a82b5c66255f0be53e107acb2d -->
## api/projects/{project}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"est"}'

```

```javascript
const url = new URL(
    "http://localhost/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "est"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "name": "rerum",
        "created_at": null,
        "updated_at": null,
        "todos": {
            "data": []
        }
    }
}
```

### HTTP Request
`PUT api/projects/{project}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Name of project.
    
<!-- END_ef65c5a82b5c66255f0be53e107acb2d -->

<!-- START_70c859bdcb978e6cdba659235c2083d3 -->
## api/projects/{project}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/projects/{project}`


<!-- END_70c859bdcb978e6cdba659235c2083d3 -->

#Todo Calls


<!-- START_0f7f51eaba392eca52cc288a3b78f293 -->
## api/todos
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/todos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/todos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "description": "Ut et quidem non.",
            "done": true,
            "created_at": null,
            "updated_at": null,
            "user": {
                "data": {
                    "id": 55,
                    "name": "Nella Wyman",
                    "email": "odell.kassulke@example.org",
                    "email_verified_at": "2020-07-21T15:37:28.000000Z",
                    "created_at": "2020-07-21T15:37:28.000000Z",
                    "updated_at": "2020-07-21T15:37:28.000000Z",
                    "todos": {
                        "data": []
                    }
                }
            },
            "project": {
                "data": {
                    "id": 54,
                    "name": "et",
                    "created_at": "2020-07-21T15:37:28.000000Z",
                    "updated_at": "2020-07-21T15:37:28.000000Z",
                    "todos": {
                        "data": []
                    }
                }
            }
        },
        {
            "id": null,
            "description": "Molestiae vero soluta est laborum dolorum mollitia earum unde.",
            "done": false,
            "created_at": null,
            "updated_at": null,
            "user": {
                "data": {
                    "id": 56,
                    "name": "Alec Mohr",
                    "email": "rgerlach@example.com",
                    "email_verified_at": "2020-07-21T15:37:28.000000Z",
                    "created_at": "2020-07-21T15:37:28.000000Z",
                    "updated_at": "2020-07-21T15:37:28.000000Z",
                    "todos": {
                        "data": []
                    }
                }
            },
            "project": {
                "data": {
                    "id": 55,
                    "name": "ex",
                    "created_at": "2020-07-21T15:37:28.000000Z",
                    "updated_at": "2020-07-21T15:37:28.000000Z",
                    "todos": {
                        "data": []
                    }
                }
            }
        }
    ]
}
```

### HTTP Request
`GET api/todos`


<!-- END_0f7f51eaba392eca52cc288a3b78f293 -->

<!-- START_a1c76b0acff2b7ead71afd6335e827a2 -->
## api/todos/{todo}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/todos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/todos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "description": "Odit perspiciatis saepe eos culpa.",
        "done": false,
        "created_at": null,
        "updated_at": null,
        "user": {
            "data": {
                "id": 57,
                "name": "Prof. Johnpaul Wilkinson IV",
                "email": "flavio48@example.net",
                "email_verified_at": "2020-07-21T15:37:28.000000Z",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        },
        "project": {
            "data": {
                "id": 56,
                "name": "numquam",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        }
    }
}
```

### HTTP Request
`GET api/todos/{todo}`


<!-- END_a1c76b0acff2b7ead71afd6335e827a2 -->

<!-- START_41fa129331503ccf982cc1646d20c4c9 -->
## api/todos
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://localhost/api/todos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"description":"odit","done":false,"project_id":10}'

```

```javascript
const url = new URL(
    "http://localhost/api/todos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "description": "odit",
    "done": false,
    "project_id": 10
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "description": "Eum dolorem et in.",
        "done": false,
        "created_at": null,
        "updated_at": null,
        "user": {
            "data": {
                "id": 58,
                "name": "Alvina Trantow",
                "email": "jacynthe92@example.com",
                "email_verified_at": "2020-07-21T15:37:29.000000Z",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        },
        "project": {
            "data": {
                "id": 57,
                "name": "sint",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        }
    }
}
```

### HTTP Request
`POST api/todos`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `description` | string |  required  | Description of todo.
        `done` | boolean |  required  | Status of todo.
        `project_id` | integer |  required  | Related project.
    
<!-- END_41fa129331503ccf982cc1646d20c4c9 -->

<!-- START_bcc6c0528293d9625ceec2fe14127c52 -->
## api/todos/{todo}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://localhost/api/todos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"description":"aut","done":true,"project_id":1}'

```

```javascript
const url = new URL(
    "http://localhost/api/todos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "description": "aut",
    "done": true,
    "project_id": 1
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "description": "Deleniti debitis magnam repudiandae.",
        "done": false,
        "created_at": null,
        "updated_at": null,
        "user": {
            "data": {
                "id": 59,
                "name": "Kelsi Walsh",
                "email": "brendan.fay@example.org",
                "email_verified_at": "2020-07-21T15:37:29.000000Z",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        },
        "project": {
            "data": {
                "id": 58,
                "name": "ratione",
                "created_at": "2020-07-21T15:37:29.000000Z",
                "updated_at": "2020-07-21T15:37:29.000000Z",
                "todos": {
                    "data": []
                }
            }
        }
    }
}
```

### HTTP Request
`PUT api/todos/{todo}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `description` | string |  optional  | Description of todo.
        `done` | boolean |  optional  | Status of todo.
        `project_id` | integer |  optional  | Related project.
    
<!-- END_bcc6c0528293d9625ceec2fe14127c52 -->

<!-- START_2b86914d270b2a6522bf69a7d55bf3d1 -->
## api/todos/{todo}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/todos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/todos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/todos/{todo}`


<!-- END_2b86914d270b2a6522bf69a7d55bf3d1 -->

#User Calls


<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## api/users
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "name": "Mrs. Odessa Yost",
            "email": "melany.strosin@example.com",
            "email_verified_at": "2020-07-21T15:37:29.000000Z",
            "created_at": null,
            "updated_at": null,
            "todos": {
                "data": []
            }
        },
        {
            "id": null,
            "name": "Mr. Enrique Bins",
            "email": "raymond.altenwerth@example.com",
            "email_verified_at": "2020-07-21T15:37:29.000000Z",
            "created_at": null,
            "updated_at": null,
            "todos": {
                "data": []
            }
        }
    ]
}
```

### HTTP Request
`GET api/users`


<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
## api/users/{user}
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": null,
        "name": "Quentin Anderson",
        "email": "yspencer@example.net",
        "email_verified_at": "2020-07-21T15:37:29.000000Z",
        "created_at": null,
        "updated_at": null,
        "todos": {
            "data": []
        }
    }
}
```

### HTTP Request
`GET api/users/{user}`


<!-- END_8653614346cb0e3d444d164579a0a0a2 -->


